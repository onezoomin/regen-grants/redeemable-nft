# Compare 721 contracts - ERC vs CW 

CW721 is "[based on Ethereum's ERC721 standard, with some enhancements](https://github.com/CosmWasm/cw-nfts/blob/main/packages/cw721/README.md#cw721-spec-non-fungible-tokens)"

Below we dive into the:
- basic functionality of 721 contracts
- enhancements of CW721
- the structural and syntactical details

### Base Functionality

CW721 : "[ownership, transfers, and allowances](https://github.com/CosmWasm/cw-nfts/blob/main/packages/cw721/README.md#base)"
ERC721 : "[balance, ownership, transfer and approval](https://docs.openzeppelin.com/contracts/4.x/api/token/erc721#IERC721)"

### CW721 enhancements

wip

### Detailed Examples

#### Tokens by Owner
Full trace of code related to getting the nft token balance for a specific owner address

ERC offers an integer balance, even without the enumerable extension:
[balanceOf() ERC721](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v4.5.0/contracts/token/ERC721/ERC721.sol#L62)
```sol
function balanceOf(address owner) public view virtual override returns (uint256) {
    require(owner != address(0), "ERC721: balance query for the zero address");
    return _balances[owner];
}
```
ERC has a way to get a single token (but not all owned tokens) with the enumerable extension:
[tokenOfOwnerByIndex() ERC721](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v4.5.0/contracts/token/ERC721/extensions/ERC721Enumerable.sol#L37-L40)
```sol
/**
* @dev See {IERC721Enumerable-tokenOfOwnerByIndex}.
*/
function tokenOfOwnerByIndex(address owner, uint256 index) public view virtual override returns (uint256) {
    require(index < ERC721.balanceOf(owner), "ERC721Enumerable: owner index out of bounds");
    return _ownedTokens[owner][index];
}
```


CW721 does not have a numeric balance fx, nor does it have a single token by index.
CW721 does have a way to get all tokens owned by specific address:
[Tokens query  CW721](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/packages/cw721/src/query.rs#L52)
```rs
/// With Enumerable extension. 
/// Returns all tokens owned by the given address, [] if unset.
/// Return type: TokensResponse.
Tokens {
    owner: String,
    start_after: Option<String>,
    limit: Option<u32>,
},
```
Exactly the same code is duplicated in msg and query...
[Tokens msg  CW721](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/contracts/cw721-base/src/msg.rs#L130-L137)
```rs
/// With Enumerable extension.
/// Returns all tokens owned by the given address, [] if unset.
/// Return type: TokensResponse.
Tokens {
    owner: String,
    start_after: Option<String>,
    limit: Option<u32>,
},
```
[Tokens rs helper  CW721](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/contracts/cw721-base/src/helpers.rs#L143-L157)
```rs
/// With enumerable extension
pub fn tokens<T: Into<String>>(
    &self,
    querier: &QuerierWrapper,
    owner: T,
    start_after: Option<String>,
    limit: Option<u32>,
) -> StdResult<TokensResponse> {
    let req = QueryMsg::Tokens {
        owner: owner.into(),
        start_after,
        limit,
    };
    self.query(querier, req)
}
```

see also: 
- [schema for the query](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/contracts/cw721-base/schema/query_msg.json#L209)
- [ts helper type definition](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/contracts/cw721-base/helpers.ts#L229)
- [Tokens ts helper  CW721](https://github.com/CosmWasm/cw-nfts/blob/v0.11.1/contracts/cw721-base/helpers.ts#L301-L304)
```ts
// list all token_ids that belong to a given owner
const tokens = async (owner: string, start_after?: string, limit?: number): Promise<TokensResponse> => {
    return client.queryContractSmart(contractAddress, { tokens: { owner, start_after, limit } });
}
```
