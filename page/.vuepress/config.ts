import { path } from '@vuepress/utils'
import markdownInclude from 'markdown-it-include'
import markdownItDec from 'markdown-it-decorate'
import WindiCSS from 'vite-plugin-windicss'

const BASE_URL = process.env.BASE_URL ?? '/'
console.log({
  BASE_URL,
})
module.exports = {
  // site config
  base: BASE_URL,
  lang: 'en-US',
  title: 'Strategies for Cosmos NFTs',
  description: 'Making sense of the different options for implementing NFTs on Cosmos chains',

  // theme and its config
  theme: path.resolve(__dirname, './theme'),
  alias: {
    // use same footer for homepage
    '@theme/HomeFooter.vue': path.resolve(__dirname, './theme/components/PageFooter.vue'),
  },
  plugins: [
    [
      '@vuepress/register-components',
      {
        componentsDir: path.resolve(__dirname, './components'),
      },
    ],
  ],
  themeConfig: {
    // https://v2.vuepress.vuejs.org/reference/default-theme/config.html#basic-config //
    themePlugins: {
      container: true, // needed for using <Badge /> etc...
    },
    logo: 'https://gitlab.com/uploads/-/system/group/avatar/15479627/OneZoomInZlogo2Corners_2x.png?width=36',
    repo: 'https://gitlab.com/onezoomin/regen-grants/redeemable-nft',
    docsDir: 'page',
    docsBranch: 'trunk', // I blame @gotjoshua
    // ****************************** ^^ I take full responsibility

    // NAV //
    navbar: [
      {
        text: 'Top Level Test Page',
        link: '/test.html',
      },
      {
        text: 'CW vs ERC',
        link: '/cw-vs-erc-721',
      },
      // NavbarGroup
      {
        text: 'NFT Options',
        children: [
          { text: 'Go Module', link: '/section/go.md' },
          { text: 'CosmWasm', link: '/section/cw.md' },
          { text: 'ETH VM', link: '/section/evm.md' },
        ],
      },
    ],
  },

  // Markdown extensions //
  extendsMarkdown: (md: any) => {
    md.set({ html: true })
    md.use(markdownInclude, 'page/' /* otherwise it's relative to root */)
    md.use(markdownItDec)
  },

  bundlerConfig: {
    viteOptions: {
      plugins: [
        WindiCSS({
          preflight: false, // disable style resetting https://windicss.org/guide/extractions.html#preflight

          // preflight: { // also possible to reset selectively via:
          //   safelist: 'h1 h2 h3 p img',
          // },

          root: path.resolve(__dirname, '..'), // = page/, default would be .temp/vite-root
          config: '.vuepress/windi.config.ts',
          // onConfigResolved: (config: WindiCssOptions, configFilePath?: string) => {
          //   console.log(config, configFilePath)
          // },

          scan: {
            dirs: ['.', '.vuepress'], // default would be ['src']
          },
        }),
      ],
    },
  },
}
