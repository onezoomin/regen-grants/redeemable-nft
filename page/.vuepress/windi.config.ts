// eslint-disable-next-line no-unused-vars
// import defaultTheme from 'windicss/defaultTheme'
// import typography from 'windicss/plugin/typography'
// import plugin from 'windicss/plugin'

export default {
  attributify: { // mega sexy https://windicss.org/integrations/vite.html#attributify-mode
    prefix: 'w:',
  },

  // https://windicss.org/posts/v30.html#alias-config
  alias: {
    lnkbh: 'box-border border border-solid border-transparent hover:border-current', // usable via *lnkbh --> will replace the alias with all classes in the dom
  },
  shortcuts: {
    'link-border-hover': 'box-border border border-solid border-transparent hover:border-current', // usable via link-border-hover --> will only show the shortcut in the dom
  },

  // safelist: 'lnkbh',
  // darkMode: false,
  // plugins: [
  //   // typography
  //   plugin(({ addBase, addVariant }) => {
  //     // addBase({
  //     //   '.link-border': {
  //     //     boxSizing: 'border-box',
  //     //     border: '1px solid transparent',
  //     //     '&:hover': {
  //     //       borderColor: 'currentColor',
  //     //     },
  //     //   },
  //     // })
  //     addVariant('link-border', ({ modifySelectors }) => {
  //       return modifySelectors(({ className }) => {
  //         return `.box-border .border .border-solid  .${className}`
  //       })
  //     })
  //   }),
  // ],
}
