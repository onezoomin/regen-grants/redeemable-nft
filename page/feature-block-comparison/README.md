# Compare Features Custom Layouts

### inline features: 
<!-- {h3:class="overline"} -->
<!-- { .bg-red-500 .this-will-be-on-the-inner-a-tag } -->

Paragraph that should be blue via decorator
<!-- {class="text-blue-500 dark:text-blue-500"} -->

<Features>
    <Feature>
        Pros <Badge class="underline" type="tip" text="2" vertical="top" />
        <template #details>
            Fastest, most secure
        </template>
    </Feature>
    <Feature>
        Neutral
        <template #details>
            GoLang
        </template>
    </Feature>
    <Feature>
        Cons <Badge type="danger" text="1+" vertical="top" />
        <template #details>
            requires chain update via governance
        </template>
    </Feature>
</Features>



### page features:

<PageFeatures :features="[
  {title:'Pros',details:'Fastest, most secure'},
  {title:'Neutral',details:'GoLang'},
  {title:'Cons',details:'requires chain update via governance'},
]" />

