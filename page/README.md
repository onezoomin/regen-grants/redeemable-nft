---
home: true
heroImage: https://gitlab.com/uploads/-/system/group/avatar/15479627/OneZoomInZlogo2Corners_2x.png?width=64
tagline: Making sense of the different options for implementing NFTs on Cosmos chains
actionText: Quick Something
actionLink: /guide/
---

<Features>
    <Feature href="#core-go-module">
        GoMod
        <template #details>
            Using Core Go modules
        </template>
    </Feature>
    <Feature href="#cosmwasm-smart-contracts">
        CosmWasm
        <template #details>
            Using Smart Contracts such as CW 721
        </template>
    </Feature>
    <Feature href="#evm-on-cosmos">
        EVM
        <template #details>
            Deploying ERC 721 on a Cosmos EVM
        </template>
    </Feature>
</Features>

# Current State of Affairs 
<!-- {h1:.text-center} -->

<div class="min-h-11/12">

!!!include(./section/go.md)!!!
</div>
<!-- empty line between these is important otherwise the heading of the below one will break -->

<div class="min-h-500px">
!!!include(./section/cw.md)!!!
</div>

!!!include(./section/evm.md)!!!