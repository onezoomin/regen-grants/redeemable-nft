## Core Go Module
<!-- {h2:.w-max .pr-4} -->

There is a core cosmos-sdk module [that will be (or has been) merged](https://github.com/cosmos/cosmos-sdk/pull/10709) Q1 2022.<br>

This is a great choice for a chain if: 
<!-- {p:.mb-0} -->
 - CosmWasm contracts will not be enabled
 - NFT functionality will be a core offering of the chain 
<!-- {ul:.mt-0 .ml-4} -->

<!-- ## Pros and Cons -->
<Features>
    <Feature>
        Pros <Badge type="tip" text="2" vertical="top" />
        <template #details>
            Fastest, most secure
        </template>
    </Feature>
    <Feature>
        Neutral
        <template #details>
            GoLang
        </template>
    </Feature>
    <Feature>
        Cons <Badge type="danger" text="1+" vertical="top" />
        <template #details>
            requires chain update via governance
        </template>
    </Feature>
</Features>