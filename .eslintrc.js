module.exports = {
  extends: [
    'standard-with-typescript',
    'plugin:vue/vue3-recommended',
  ],
  parser: 'vue-eslint-parser', // https://eslint.vuejs.org/user-guide/#usage
  parserOptions: {
    parser: '@typescript-eslint/parser',
    project: './tsconfig.json',
    extraFileExtensions: ['.vue'],
  },
  rules: {
    // preference
    'comma-dangle': ['error', 'always-multiline'],
    'arrow-parens': [2, 'as-needed', { requireForBlockBody: true }], // allow paren-less arrow functions

    // reduce unnecessary errors
    'no-unused-vars': 'warn',
    'vue/no-unused-vars': 'warn',
    'vue/multi-word-component-names': 'warn',
    '@typescript-eslint/strict-boolean-expressions': 'warn',
    '@typescript-eslint/prefer-nullish-coalescing': 'warn',

    // allow debugger during development only
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
}
