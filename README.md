# Strategies for NFT implementations on Cosmos

[https://onezoomin.gitlab.io/regen-grants/redeemable-nft/](https://onezoomin.gitlab.io/regen-grants/redeemable-nft/)
## Roadmap
TODO: Roadmap

## Contributing
PRs for updating / adapting information is highly welcome - let's make this an up-to-date resource for NFTs on Cosmos!

## Authors and acknowledgment
- Joshua ([@gotjoshua](https://gitlab.com/gotjoshua))
- Manuel ([@tennox](https://dev.page/tennox))
- [Regen Network](https://regen.network) (funding) ♥

## License
Code (AGPL):
![AGPL](https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/AGPLv3_Logo.svg/320px-AGPLv3_Logo.svg.png)

Content (CC BY-NC-SA):
![CC BY-NC-SA](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png)
